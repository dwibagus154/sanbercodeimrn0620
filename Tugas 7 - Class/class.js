

// soal 1
class Animal {
    // Code class di sini
    constructor(name){
    	this.name = name;
    	this.legs = 4;
    	this.cold_blooded = false;
    }
    name(){
    	return this.name;
    }
    legs(){
    	return this.legs;
    }
    cold_blooded(){
    	return this.cold_blooded;
    }
}

var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Ape extends Animal {
  constructor(name) {
    super(name);
    this.yell1 = "Auooo";
  }
  yell() {
    console.log (this.yell1);
  }
}

var sugokong = new Ape("kera sakti");
sugokong.yell();

class Frog extends Animal {
  constructor(name) {
    super(name);
    this.jump1 = "hop hop";
  }
  jump() {
    console.log (this.jump1);
  }
}

var kodok = new Frog("buduk");
kodok.jump();

// soal 2

// function Clock({ template }) {
  
//   var timer;

//   function render() {
//     var date = new Date();

//     var hours = date.getHours();
//     if (hours < 10) hours = '0' + hours;

//     var mins = date.getMinutes();
//     if (mins < 10) mins = '0' + mins;

//     var secs = date.getSeconds();
//     if (secs < 10) secs = '0' + secs;

//     var output = template
//       .replace('h', hours)
//       .replace('m', mins)
//       .replace('s', secs);

//     console.log(output);
//   }

//   this.stop = function() {
//     clearInterval(timer);
//   };

//   this.start = function() {
//     render();
//     timer = setInterval(render, 1000);
//   };

// }

// var clock = new Clock({template: 'h:m:s'});
// clock.start();

class Clock {

	constructor({template}){
    this.template = template

    this.timer;
	}

  render(){
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = this.template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);

  }

  stop(){
    clearInterval(this.timer);
  }

  start(){
    // this.render.bind(this);
    this.timer = setInterval(this.render.bind(this), 1000);
  }
	
    
}

var clock = new Clock({template: 'h:m:s'});
clock.start();