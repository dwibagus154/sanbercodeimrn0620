import * as React from 'react';
import { Button, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Login from '../../Tugas/TugasNavigation/LoginScreen'
import Skill from '../../Tugas/TugasNavigation/SkillScreen'

import Login3 from '../../Quiz 3/LoginScreen'
import Home from '../../Quiz 3/HomeScreen'




// function HomeScreen() {
//     return (
//         <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
//             <Text>Home Screen</Text>
//         </View>
//     );
// }

const Stack = createStackNavigator();

function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Login" headerMode="none">
                <Stack.Screen name="Login" component={Login3} />
                <Stack.Screen name="Details" component={Home} />
            </Stack.Navigator>
            {/* <Stack.Navigator>
                <Stack.Screen name="Details" component={DetailsScreen} />
            </Stack.Navigator> */}
        </NavigationContainer>
    );
}

function HomeScreen({ navigation }) {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Home Screen</Text>
            <Button
                title="Go to Details"
                onPress={() => navigation.navigate('Details')}
            />
        </View>
    );
}
function DetailsScreen({ navigation }) {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Details Screen</Text>
            <Button
                title="Go to Details... again"
                onPress={() => navigation.push('Details')}
            />
            {/* <Button title="Go to Home" onPress={() => navigation.navigate('Home')} /> */}
            <Button title="Go back" onPress={() => navigation.goBack()} />
        </View>
    );
}

export default App;