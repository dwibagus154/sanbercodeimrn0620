import React, { useState, Component } from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import data from './data.json';

const DEVICE = Dimensions.get('window')

export default class HomeScreen extends React.Component {

  render() {
    console.log(data)
    return (
      <View style={styles.container}>
        <View style={{ minHeight: 50, width: DEVICE.width * 0.88 + 20, marginVertical: 8 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text>Hai,{'\n'}
              <Text style={styles.headerText}>{this.props.route.params.userName}</Text>
            </Text>
          </View>

          <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
            <Button title='My Profile' color='blue' onPress={
              () => { this.props.navigation.navigate('Profile') }

            } />
            <Button title='Detail Film' color='blue' onPress={
              () => { this.props.navigation.navigate('Detail') }

            } />
          </View>

        </View>
        <FlatList
          data={data.Search}
          renderItem={({ item }) =>
            <TouchableOpacity>
              <ListItem data={item} />
            </TouchableOpacity>}
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={() => <View style={{ height: 5, backgroundColor: "#F5F5F5" }} />}
          numColumns={2}
        />



      </View>
    )
  }
};

class ListItem extends React.Component {

  render() {
    const data = this.props.data
    // const { data, update } = this.props;
    return (
      <View style={styles.itemContainer}>
        <Image source={{ uri: data.Poster }} style={styles.itemImage} resizeMode='contain' />
        <View style={{ alignItems: "center" }}>
          <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{data.Title}</Text>
          <Text style={styles.itemType}>{data.Type}</Text>
          <Text>Tahun: {data.Year}</Text>

        </View>
      </View>
    )
  }
};



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',

  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold'
  },

  itemContainer: {
    width: DEVICE.width * 0.44,

  },
  itemImage: {
    height: 150,

  },
  itemName: {
    fontWeight: "bold"

  },
  itemType: {
    color: "blue"
  },

})
