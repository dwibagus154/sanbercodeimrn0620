import React from 'react'
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native'

import data from './data.json';

const DEVICE = Dimensions.get('window')

export default class Detailitem extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    data={data.Search}
                    renderItem={({ item }) => <ListItem data={item} />}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={() => <View style={{ height: 5, backgroundColor: "#F5F5F5" }} />}
                />

            </View>
        )
    }
}

class ListItem extends React.Component {

    render() {
        const data = this.props.data
        // const { data, update } = this.props;
        return (
            <View style={styles.itemContainer}>
                <View style={{ alignItems: "center", flex: 1, width: 300 }}>
                    <Image source={{ uri: data.Poster }} style={styles.itemImage} resizeMode='contain' />
                </View>
                <View style={{ alignItems: "flex-start", flex: 1 }}>
                    <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{data.Title}</Text>
                    <Text>Rating    : <Text style={styles.itemType}>{data.imdbRating}</Text></Text>
                    <Text>Actiors   : <Text style={styles.itemType}>{data.Actors}</Text></Text>
                    <Text>Plot      : <Text style={styles.itemType}>{data.Plot}</Text></Text>

                </View>
            </View>
        )
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',

    },
    headerText: {
        fontSize: 18,
        fontWeight: 'bold'
    },

    itemContainer: {
        flexDirection: "row",
        width: 400


    },
    itemImage: {
        height: 250,
        width: 200,

    },
    itemName: {
        fontWeight: "bold"

    },
    itemType: {
        color: "blue"
    }

})