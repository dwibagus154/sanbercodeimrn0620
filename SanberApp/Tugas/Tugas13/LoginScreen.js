import React, { Component } from 'react'
import {
    Platform,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    FlatList,
    Text
} from 'react-native'

import Icon from 'react-native-vector-icons/MaterialIcons';


export default class LoginScreen extends Component {
    render() {
        return (
            <View style={styles.container}>

                <View style={styles.logo} >
                    <Image source={require('./../Images/itb.png')} style={{ width: 98, height: 98 }} />

                </View>

                <View style={styles.body}>
                    <View style={styles.subBody}>
                        <View style={{ flex: 1 }}>
                            <Text style={{ marginTop: 60, marginLeft: 30, fontSize: 18 }}>Username :</Text>
                        </View>
                        <View style={{ height: 30, backgroundColor: "rgba(240, 235, 235, 1)", margin: 40 }}>

                        </View>
                    </View>
                    <View style={styles.subBody}>
                        <View style={{ flex: 1 }}>
                            <Text style={{ marginTop: 10, marginLeft: 30, fontSize: 18 }}>Password :</Text>
                        </View>
                        <View style={{ height: 30, backgroundColor: "rgba(240, 235, 235, 1)", margin: 40, marginBottom: 90 }}>

                        </View>
                    </View>
                    <View style={styles.subBody}>
                        <View style={{ height: 30, backgroundColor: "skyblue", margin: 70, alignItems: 'center', justifyContent: 'center', marginTop: 0 }}>
                            <Text style={{ fontSize: 18 }}>LOGGIN</Text>
                        </View>
                    </View>

                </View>

                <View style={styles.footer}>
                    <Text style={{ fontSize: 14 }}>Don't Have an Acoount? </Text>
                    <Text style={{ color: 'blue', fontSize: 16 }}> Sign Up</Text>
                </View>

            </View>
        );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: 'red'
    },
    logo: {
        marginTop: 120,
        height: 120,
        backgroundColor: 'white',
        alignItems: "center"
    },
    body: {
        flex: 1,
    },

    footer: {
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
        height: 50,

    },
    subBody: {
        flex: 1,

    }
})