import React, { Component } from 'react'
import {
    Platform,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    FlatList,
    Text,
    TextInput,
    ScrollView,
    Button,
    Alert
} from 'react-native'

import Icon from 'react-native-vector-icons/MaterialIcons';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';



export default class App extends Component {
    render() {
        return (
            <View style={styles.container}>

                <View style={styles.logo} >
                    <View style={{ flex: 1, alignItems: "center" }}>
                        <Text style={{ fontSize: 34, }}>About Me</Text>
                        <Image source={require('./../Images/default.jpg')} style={{ width: 120, height: 120, borderRadius: 70, marginTop: 5 }} />
                    </View>
                    <View style={{ alignItems: "center" }}>
                        <Text style={{ fontSize: 24 }}>Dwi Bagus</Text>
                        <Text style={{ fontSize: 18, color: "blue" }}>Web & Mobile Development</Text>
                    </View>
                </View>



                <View style={styles.body}>
                    <Text style={{ fontSize: 18, }}>Portofolio</Text>
                    <Text style={{ marginTop: -5, alignItems: "center" }}>_____________________________________________________</Text>
                    <View style={{ alignItems: "center" }}>
                        <Image source={require('./../Images/gitlab.png')} style={{ width: 40, height: 40, borderRadius: 70, marginTop: 5 }} />

                        <Text style={{ fontSize: 18, margin: "auto", justifyContent: "center" }}>dwibagus154</Text>
                    </View>
                </View>

                <View style={styles.footer}>
                    <Text style={{ fontSize: 18, }}>Hubungi Saya</Text>
                    <Text style={{ marginTop: -5, alignItems: "center" }}>_____________________________________________________</Text>
                    <View style={{ justifyContent: "space-around", flexDirection: "row", marginTop: 20 }}>
                        <View style={{ alignItems: "center" }}>
                            <Image source={require('./../Images/gmail.png')} style={{ width: 40, height: 40, borderRadius: 70, marginTop: 5 }} />
                            <Text style={{ fontSize: 18, margin: "auto" }}>dwibagus154@gmail.com</Text>
                        </View>
                        <View style={{ alignItems: "center" }}>
                            <Image source={require('./../Images/instagram.png')} style={{ width: 40, height: 40, borderRadius: 70, marginTop: 5 }} />
                            <Text style={{ fontSize: 18, margin: "auto" }}>dwibgst</Text>
                        </View>

                    </View>
                </View>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', flexDirection: "row" }}>
                    <Navigator onPress={() => this.props.navigation.navigate('Proyek')} />
                    <Navigator2 onPress={() => this.props.navigation.navigate('Details')} />
                    <Navigator1 onPress={() => this.props.navigation.navigate('Tambah')} />

                </View>

            </View>

        )
    }
}

function Navigator({ onPress }) {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>

            <Button
                title="Proyek"
                onPress={onPress}

            />
        </View>
    );
}

function Navigator1({ onPress }) {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>

            <Button
                title="Tambah"
                onPress={onPress}

            />
        </View>
    );
}

function Navigator2({ onPress }) {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>

            <Button
                title="Skill"
                onPress={onPress}

            />
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    logo: {
        marginTop: 100,
        height: 260,
        backgroundColor: 'white',
        alignItems: "center"
    },
    body: {
        flex: 1,
        backgroundColor: "rgba(240, 235, 235, 1)",
        margin: 5,
        padding: 10,
        borderRadius: 20,
    },

    footer: {
        // alignItems: "center",
        // justifyContent: "center",
        // flexDirection: "row",
        padding: 10,
        height: 180,
        backgroundColor: "rgba(240, 235, 235, 1)",
        marginBottom: 50,
        margin: 5,
        borderRadius: 20
    },
    subBody: {
        flex: 1,

    }
})