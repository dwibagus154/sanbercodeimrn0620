import * as React from 'react';
import { Button, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Login from '../TugasNavigation/LoginScreen'
import Skill from '../TugasNavigation/SkillScreen'
import Proyek from '../TugasNavigation/ProjectScreen'
import Tambah from '../TugasNavigation/AddScreen'
import About from '../TugasNavigation/AboutScreen'
import Login3 from '../../Quiz 3/LoginScreen'
import Home from '../../Quiz 3/HomeScreen'


// function HomeScreen() {
//     return (
//         <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
//             <Text>Home Screen</Text>
//         </View>
//     );
// }

const Stack = createStackNavigator();

function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Login" headerMode="none">
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="Details" component={Skill} />
                <Stack.Screen name="Proyek" component={Proyek} />
                <Stack.Screen name="Tambah" component={Tambah} />
                <Stack.Screen name="About" component={About} />
                {/* <Stack.Screen name="Login" component={Login3} />
                <Stack.Screen name="Details" component={Home} /> */}
            </Stack.Navigator>
            {/* <Stack.Navigator>
                <Stack.Screen name="Details" component={DetailsScreen} />
            </Stack.Navigator> */}
        </NavigationContainer>
    );
}

function HomeScreen({ navigation }) {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Home Screen</Text>
            <Button
                title="LOGGIN"
                onPress={() => navigation.navigate('Details')}
            />
        </View>
    );
}
function DetailsScreen({ navigation }) {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Details Screen</Text>
            <Button
                title="Go to Details... again"
                onPress={() => navigation.push('Details')}
            />
            {/* <Button title="Go to Home" onPress={() => navigation.navigate('Home')} /> */}
            <Button title="Go back" onPress={() => navigation.goBack()} />
        </View>
    );
}

export default App;