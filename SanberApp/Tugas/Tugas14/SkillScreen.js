import React, { Component } from 'react'
import {
    Platform,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    FlatList,
    Text,
    ScrollView,
    Button
} from 'react-native'

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Data from './skillData.json'

const Skill = (props) => {
    return (
        <View style={styles.skillList}>
            <View style={{ justifyContent: "center" }}>
                <Icon name={props.data.iconName} size={100} />
            </View>
            <View style={{ flex: 1 }}>
                <View style={{ alignItems: "flex-end" }}>
                    <Text style={{ fontSize: 26 }}>{props.data.skillName}</Text>
                    <Text style={{ fontSize: 18, color: "darkblue" }}>{props.data.categoryName}</Text>
                </View>
                <View style={{}}>
                    <Text style={{ fontSize: 60, marginLeft: 55, color: "white" }}>{props.data.percentageProgress}</Text>
                </View>


            </View>


            <View style={{ marginRight: -55, justifyContent: "center" }}>
                <Icon name="chevron-right" size={180} />
            </View>




        </View>

    )
}


export default class SkillScreen extends Component {
    render() {
        // alert(Data.items[0].skillName)
        return (
            <View style={styles.container}>

                <View style={styles.logo} >
                    <Image source={require('./../Images/itb.png')} style={{ width: 60, height: 60 }} />

                </View>
                <View style={{ flexDirection: "row", marginTop: -25, marginLeft: 10 }}>
                    <Image source={require('./../Images/default.jpg')} style={{ width: 30, height: 30, borderRadius: 70, marginTop: 5 }} />
                    <View>
                        <Text style={{ fontSize: 12, marginLeft: 5 }}>Hai</Text>
                        <Text style={{ fontSize: 16, marginLeft: 5 }}>Dwi Bagus</Text>
                    </View>

                </View>
                <View style={{ marginTop: 20, marginLeft: 30, }}>
                    <Text style={{ color: 'blue', fontSize: 32 }}>
                        SKILL
                    </Text>
                </View>
                <View style={{ borderStyle: "solid", height: 3, backgroundColor: "skyblue", marginLeft: 15, marginRight: 15 }}>

                </View>

                <View style={styles.skillTitle}>
                    <View style={styles.skillPart}>
                        <Text style={{ fontSize: 18 }}>
                            Framework
                        </Text>
                    </View>
                    <View style={styles.skillPart}>
                        <Text style={{ fontSize: 16 }}>
                            Bahasa Program
                        </Text>
                    </View>
                    <View style={styles.skillPart}>
                        <Text style={{ fontSize: 18 }}>
                            Teknologi
                        </Text>
                    </View>
                </View>

                <View style={styles.body}>
                    <ScrollView>

                        <FlatList
                            data={Data.items}
                            renderItem={(data) => <Skill data={data.item} />}
                            keyExtractor={(item) => item.id}
                            ItemSeparatorComponent={() => <View style={{ height: 3, backgroundColor: "#F5F5F5" }} />}

                        />
                    </ScrollView>


                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: 'red'
    },
    logo: {
        marginRight: 10,
        marginTop: 50,
        height: 120,
        backgroundColor: 'white',
        alignItems: "flex-end"
    },
    body: {
        flex: 1,
    },

    footer: {
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
        height: 50,

    },
    subBody: {
        flex: 1,

    },
    skillTitle: {
        flexDirection: "row",
        height: 50,
        margin: 5,
    },
    skillPart: {
        backgroundColor: "skyblue",
        flex: 1,
        margin: 5,
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center"
    },
    skillList: {
        height: 150,
        padding: 5,
        backgroundColor: "#B4E9FF",
        marginLeft: 5,
        marginRight: 5,
        borderRadius: 10,
        flexDirection: "row",
    }
})