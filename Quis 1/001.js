
function bandingkan(num1 = 0, num2 = 0) {
  // code di sini	
  // cek jika negatif
  if (Number(num1) < 0 || Number(num2) < 0){
  	return -1 ;
  }else { 
  	// cek yang mana lebih besar
  	if (Number(num1) > Number(num2)){
	  	return Number(num1) ;
	}else if (Number(num1) == Number(num2)){
		return -1;
	}else{
		return Number(num2) ;
	} 
  }
}

function balikString(input) {
  // Silakan tulis code kamu di sini
  var a = '';
  // looping dengan mengetahui panjangnya terlebih dahulu
  for ( i = input.length -1 ; i >= 0; i--){
  	a += input[i];
  }
  return a ;
}

function palindrome(input) {
  // Silakan tulis code kamu di sini
	  var a = '';
	  for ( i = input.length -1 ; i >= 0; i--){
	  	a += input[i];
	  }
	  if (a == input){
	  	// jika katanya sama maka palindrome
	  	return true;
	  }else{
	  	return false;
	  }
}

// TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18

// TEST CASES BalikString
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

// TEST CASES Palindrome
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false