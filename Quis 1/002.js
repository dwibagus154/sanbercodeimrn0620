
function DescendingTen(num = 0) {
  // jika nilainya default
  var a = '';
  if (num == 0){
    a += -1;
  }else {
    // jika tidak 
    for (i = num ; i >num-10 ; i--){
      a += i;
      a += ' ';
    }
  }
  return a;
}

function AscendingTen(num = 0) {
  // Tulis code kamu di sini
  // jika nilainya default 
  var a = '';
  if (num == 0){
    a += -1;
  }else {
    // jika tidak
    for (i = num ; i <num+10 ; i++){
      a += i;
      a += ' ';
    }
  }
  return a;
}

function ConditionalAscDesc(reference = 0, check = 0) {
  // Tulis code kamu di sini
  var a = '';
  // jika nilainya default
  if (reference == 0 || check == 0){
    a += -1;
  }else {
    // jika tidak 
    if (check % 2 == 0){
      // jika check genap
      for (i = reference ; i >reference-10 ; i--){
        a += i;
        a += ' ';
      }
    }else {
      // jika check ganjil
      for (i = reference ; i <reference+10 ; i++){
        a += i;
        a += ' ';
      }
    }
  }
  return a
}

function ularTangga() {
  // Tulis code kamu di sini
  var a = '';
  // looping 100 -1
  for (i = 100; i > 0; i--){
    a += i;
    a += ' '; 
    // jika menemukan angka dengan satuan 1 (91, 71)maka 
    if (i % 10 == 1){
      // berikan spasi
      a += '\n';
      // jika angka itu dikurang 1 dan 20 sisa 10 seperti 90
      if ((i-1) % 20 == 10) { 
        // looping dari dikurang 10 hingga ke bilangan tersebut
        for (b = i -10 ; b <= i-1 ; b++){
          a += b;
          a += ' ';
        }
        // kasi spasi lagi 
        a += '\n'
        // lalu ganti i nya dengan dikurang 10
        i -= 10;
      }
    }
     

  }
  return a ;

}

// TEST CASES Descending Ten
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1

// TEST CASES Ascending Ten
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1

// TEST CASES Conditional Ascending Descending
console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1

// TEST CASE Ular Tangga
console.log(ularTangga())
/*
Output :
  100 99 98 97 96 95 94 93 92 91
  81 82 83 84 85 86 87 88 89 90
  80 79 78 77 76 75 74 73 72 71
  61 62 63 64 65 66 67 68 69 70
  60 59 58 57 56 55 54 53 52 51
  41 42 43 44 45 46 47 48 49 50
  40 39 38 37 36 35 34 33 32 31
  21 22 23 24 25 26 27 28 29 30
  20 19 18 17 16 15 14 13 12 11
  1 2 3 4 5 6 7 8 9 10
*/