//soal 1
console.log ("LOOPING PERTAMA");
var i = 2;
while (i<= 20){
	console.log(i + ' - I love coding');
	i+=2;
}

console.log("LOOPING KEDUA");
while (i >2){
	i-=2;
	console.log(i + ' - I will become a mobile developer');
}

// soal 2 
for (var i = 1; i <=20; i++){
	if (i % 3 == 0 && i % 2 == 1){
		console.log(i + ' - I Love Coding');
	}else if (i % 2 == 1){
		console.log (i + ' - Santai');
	}else {
		console.log (i + ' - Berkualitas');
	}
}

// soal 3
var a = '';
for (var i = 0 ; i < 4 ; i++){
	for (var j = 0; j<8; j++){
		a+= '#';
	}
	if (i != 3 ){
		a+= '\n';
	}
}
console.log(a);

// soal 4

var a = '';
for (var i = 0 ; i < 7 ; i++){
	var n = i + 1;
	for (var j = 0; j< n; j++){
		a+= '#';
	}
	if (i != 6 ){
		a+= '\n';
	}
}
console.log(a);

// soal 5

var a = '';
for (var i = 0 ; i < 8 ; i++){
	for (var j = 0; j< 8; j++){
		if ( i % 2 == 0 ){
			if ( j % 2 == 0 ) {
				a+= ' ';
			}else {
				a+= '#';
			}
		}else {
			if ( j % 2 == 0 ) {
				a+= '#';
			}else {
				a+= ' ';
			}
		}	
	}
	if (i != 7 ){
		a+= '\n';
	}
}
console.log(a);