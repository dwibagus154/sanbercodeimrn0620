// SOAL 1 
//  [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]] 
function arrayToObject(arr) {

	if (arr == []){
		console.log("")
	}else {
		var now = new Date();
		var tahun = now.getFullYear();
	    // Code di sini
	    for(i= 0; i<arr.length; i++){
	    	var a = (i+1 + '. ' + arr[i][0] + ' ' + arr[i][1] + ' : ');
	    	var orang = {};
	    	orang.firstName = arr[i][0];
	    	orang.lastName = arr[i][1];
	    	orang.gender = arr[i][2];
	    	if (arr[i][3] < tahun ){
	    		orang.age = tahun - arr[i][3];

			}else{
				orang.age =  "Invalid birth year" ;
			}
			console.log(a);
	    	console.log(orang);

	    }
	}	
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) //


//soal 2

function shoppingTime(memberId = '', money) {
  // you can only write your code here!
  // jika tidak ada member id 
  if (memberId == '' ){
  	return 'Mohon maaf, toko X hanya berlaku untuk member saja';
  }else {
  	// jika saldo kurang
  	if (money < 50000){
  		return 'Mohon maaf, uang tidak cukup'
  	}else {
  		var object = {};
  		object.memberId = memberId;
  		object.money = money;
  		object.listPurchased = [];
  		object.changeMoney = money;
  		//jika uangnya lebih besar dari 1500000 
  		var harga = [1500000, 500000, 250000, 175000, 50000]
  		for (i = 0; i< harga.length ; i ++ ){
  			if (object.changeMoney >= harga[i]){
  				var a = harga[i];
  				switch (harga[i]){
  					case 1500000: {object.listPurchased.push('Sepatu Stacattu'); break};
  					case 500000: {object.listPurchased.push('Baju Zoro'); break};
  					case 250000: {object.listPurchased.push('Baju H&N'); break};
  					case 175000: {object.listPurchased.push('Sweater Uniklooh'); break};
  					case 50000: {object.listPurchased.push('Casing Handphone'); break};
  				}
  			}
  			else{
  				var a = 0;
  			}
  			object.changeMoney -= a;
  		}
  		return object;



  	}
  }
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// soal 3
function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var array = [];
  //your code here
  for (i = 0 ; i < arrPenumpang.length ; i++){
  	switch (arrPenumpang[i][1]){
  		case 'A': {var awal = 1; break};
  		case 'B': {var awal = 2; break};
  		case 'C': {var awal = 3; break};
  		case 'D': {var awal = 4; break};
  		case 'E': {var awal = 5; break};
  		case 'F': {var awal = 6; break};
  	}
  	switch (arrPenumpang[i][2]){
  		case 'A': {var akhir = 1; break};
  		case 'B': {var akhir = 2; break};
  		case 'C': {var akhir = 3; break};
  		case 'D': {var akhir = 4; break};
  		case 'E': {var akhir = 5; break};
  		case 'F': {var akhir = 6; break};
  	}

  	var nama = {};
  	nama.penumpang = arrPenumpang[i][0];
  	nama.naikDari = arrPenumpang[i][1];
  	nama.tujuan = arrPenumpang[i][2];
  	nama.bayar = Math.abs(2000 * (akhir - awal));

  	array.push(nama);

  }
  return array;
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]

